package com.example.accounting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.core.env.Environment;

@RestController
@RequestMapping("/accounting")
public class AccountController {
    @Autowired
    private Environment env;

    @GetMapping("/account")
    public String getAccount() {
        System.out.println(env.getProperty("server.url"));
        System.out.println(env.getProperty("spring.cloud.config.uri"));
        return "Accounting";
    }

}
